<?php

function get_users(){
    $data = file_get_contents('users.json');
    if($data == false){
        echo 'データが読み込めませんでした。';
        return array();
    } else{
       $array = json_decode($data,true);
       return $array;
    }
}

function add_users($name,$pass){
  $users = get_users();
  $users [] = array('id' => $name, 'pass' => $pass);
  
  $json = json_encode($users,JSON_PRETTY_PRINT);
  file_put_contents('users.json', $json);
}

function password_check($name,$pass){
    //ユーザーリスト
    $users = get_users();
    
    foreach ($users as $user){
        if($user['id'] == $name && $user['pass'] == $pass){
            return true;//ユーザー名・パスワードが合っていた
        }
    }
    return false;//合っているものがなかった
}
//echo(password_check('aaa','aaa')) ? 'OK' : 'NG';
//echo '<br>';
//echo(password_check('fighters','fight')) ? 'OK' : 'NG';
?>

