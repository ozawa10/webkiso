<?php
$count = 1;
$sum = 0;

while ($count <= 100){
    $sum += $count;
    if($sum > 1000){
        echo '1000を超えたのでcountは' . $count . 'で終了します<br>';
        break;
    }
    $count += 1;
}
//合計を発表します。
echo 'sumは' . $sum;

//break文で階層を抜ける
//$i=1;
//while ($i<10){
//    echo 'i='.$i.' ';
//    $j=1;
//    while ($j<5){
//        echo 'j='.$j.'';
//        echo 'i*j=' . $i*$j . ' ';
//        if($i*$j>15){
//            break 2;
//        }
//        $j += 1;
//    }
//    $i += 1;
//    echo '<br>';
//}
//break文が実行された時に処理が移る位置

//for文で
//$sum = 0;
//
//for($count = 1; $count <= 100; $count++){
//    $sum += $count;
//    if($sum > 1000){
//        break;
//    }
//}
//echo 'sumは'.$sum;
?>