<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html lang="ja">
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        //四則演算をします。
        $a = 10;
        $b = 5;
        
        echo $a + $b . '<br>';
        echo $a - $b . '<br>';
        echo $a * $b . '<br>';
        echo $a / $b . '<br><br>';
        
        // '%' 演算子
        echo $a % $b . '<br>';
        echo $a % 3 . '<br><br>';
        
        // ++, --
        echo $a++ . '<br>';
        echo ++$a . '<br>';
        
        echo $b-- . '<br>';
        echo --$b . '<br>';
        ?>
    </body>
</html>
