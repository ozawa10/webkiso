<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html lang="ja">
    <head>
        <meta charset="UTF-8">
        <title>型の変換</title>
    </head>
    <body>
        <div>
        <?php
        //変数に代入します。
        $a = 123;
        //変数に文字列を代入します。
        $b = '321';
        
        echo '$a の型: ' . gettype($a) . '<br>';
        echo '$b の型: ' . gettype($b) . '<br><br>';
        
        //関数を使い型を変換します。
        $a = strval($a);
        $b = strval($b);
        
        echo '$a の型: ' . gettype($a) . '<br>';
        echo '$b の型: ' . gettype($b) . '<br><br>';
        
        //型キャストで型を変換します。
        $a = (int) $a;
        $b = (int) $b;
        
        echo '$a の型: ' . gettype($a) . '<br>';
        echo '$b の型: ' . gettype($b) . '<br><br>';
        
        $c = 'abc';
        $c = (int) $c;
        
        echo '$c の型: ' . gettype($c) . '<br>';
        echo '$c の中身: ' . $c . '<br><br>';
        ?>
        </div>
    </body>
</html>
