<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html lang="ja">
    <head>
        <meta charset="UTF-8">
        <title>定数を使用するには？</title>
    </head>
    <body>
<?php
// put your code here
define('HELLO','hello world!');
echo '定数: HELLO: ' . HELLO . '<br>';
//定数Helloは定義されていないため、Noticeエラーが発生します。
echo '定数: Hello: ' . Hello . '<br>';

//定数定義の戻り値を$resultに代入します。第３引数にtrueを指定して大文字・小文字を
//区別しないようにします。
$result = define('GREETING','こんにちは', true);
if($result){
    echo '定数: GREETING: ' . GREETING . '<br>';
    echo '定数: Greeting: ' . Greeting . '<br>';
} else {
    echo '定数の定義に失敗しました。' . '<br>';
}

// PHP5.3以降では、constキーワードを使うことでも定義できる。
const WORLD = 'World';
echo '定数: WORLD: ' . WORLD;
?>
    </body>
</html>
