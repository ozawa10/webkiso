<?php
$names = array('浅野','伊藤','宇田','江本');

echo count($names);
echo '<pre>';
var_dump($names);
echo '</pre>';
echo '<br>';

$names[] = '太田';
echo count($names);
echo '<br>';

echo '<pre>';
var_dump($names);
echo '</pre>';
echo '<br>';

echo $names[count($names)-1];
echo '<br><br>';


$empty = [];
echo count($empty);
echo '<br>';

$test = 100;
if(is_array($test)){
    echo count($test);
    echo '<br>';
}else{
    echo '配列ではありません';
}

$test = [100];
//$count = (is_array($test)) ? count($test) : 0;
if(is_array($test)){
    echo '<br>';
    echo count($test);
}else{
    echo '配列ではありません';
}
?>