<!dogtype html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
<?php
//複数のデータを設定
 $people = array(
     array(
     'name' => '野木',
     'height' => 158,
     ),
     array(
     'name' => '小林',
     'height' => 160,
     ),
     array(
     'name' => '吉岡',
     'height' => 168,
     ),
 );
 
//  echo '<pre>'; 
//  var_dump($people);
//  echo '</pre>';
//  
////tableを組む
//  echo '<table>';
//  echo '<thead>';
//  echo '<tr>';
//  echo '<td>名前</td>';
//  echo '<td>身長</td>';
//  echo '</tr>';
//  echo '</thead>';
//  echo '<tbody>';
//  
//  foreach ($people as $person){
//    echo '<tr>';
//    echo '<td>' . $person['name'] . '</td>';
//    echo '<td>' . $person['height'] . '</td>';
//    echo '</tr>';
//  }
//  echo '</tbody>';
//  echo '</table>'
?>
 <table>
 <thead>
  <tr>
     <th>名前</th>
     <th>身長</th>
  </tr>
 </thead>
 <tbody>
<?php foreach ($people as $person): ?>
     <tr>
         <td><?= $person['name']; ?></td>
         <td><?= $person['height']; ?></td>
     </tr>
<?php endforeach; ?>
 </tbody>
 </table>
   </body>
</html>
